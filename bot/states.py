from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup


class BotState(StatesGroup):
    waiting_for_email = State()
    waiting_email = State()
