import requests

BASE_URL = 'http://127.0.0.1:8000/api/v1'


def create_user(name, user_id):
    try:
        user = get_user(user_id)
        if not user:
            url = f'{BASE_URL}/bot-users/'
            data = {'name': name, 'user_id': user_id}
            r = requests.post(url, data)
            r.raise_for_status()
            return "User created successfully"
        else:
            return "User already exists"
    except requests.exceptions.RequestException as e:
        raise Exception(f"Failed to create user: {e}") from e



def put_mail(user_id, email):
    user = get_user(user_id)
    if not user:
        return "User not found"

    id = user.get('id')
    name = user.get('name')
    url = f'{BASE_URL}/bot-users/{id}/'

    data = {
        'id': id,
        'user_id': user_id,
        'name': name,
        'email': email
    }

    response = requests.put(url, json=data)

    if response.status_code == 200:
        return "User email updated successfully"
    else:
        return f"Error: {response.text}"


def free_time_create(user_id, time_data):
    url = f'{BASE_URL}/free-times/'
    response = requests.post(url, {'user_id': user_id, 'time_data': time_data})
    if response.status_code == 201:
        return "Free Time added"
    else:
        return f"Xato: {response.text}"


def get_user(user_id):
    url = f'{BASE_URL}/users/{user_id}'
    response = requests.get(url)
    print(response.text)
    if response.status_code == 200:
        user_data = response.json()
        if user_data:
            return user_data
        else:
            print("User data is empty.")
            return None
    else:
        return None


def get_free_time():
    url = f'{BASE_URL}/free-times/'
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Error: {response.status_code}")
        return []


def get_free_time_by_id(id):
    url = f'{BASE_URL}/free-times/{id}'
    print(url)
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Error: {response.status_code}")
        return []
