import datetime
import pytz
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

from bot.api import *

menu = InlineKeyboardMarkup().add(
    InlineKeyboardButton('Enter Free Time', callback_data='create_meeting'),
    InlineKeyboardButton('View users\' free time', callback_data='view_free_times')
)


def create_days_list():
    today = datetime.datetime.now(pytz.timezone('Asia/Tashkent')).date()
    schedule = []
    for i in range(0, 30, 3):
        row = []
        for j in range(3):
            date = today + datetime.timedelta(days=i + j)
            date_str = date.strftime("%Y-%m-%d")
            row.append(InlineKeyboardButton(date_str, callback_data=f"select_date_{date_str}"))
        schedule.append(row)

    confirm_button = InlineKeyboardButton("Confirm", callback_data="confirm_date")
    back = InlineKeyboardButton("Back", callback_data="menu")

    keyboard = InlineKeyboardMarkup()
    for row in schedule:
        keyboard.row(*row)
    keyboard.add(confirm_button)
    keyboard.add(back)
    return keyboard


def create_hours_list():
    hours = []
    start_time = datetime.time(8, 0)
    end_time = datetime.time(17, 0)
    interval = datetime.timedelta(hours=1)
    current_time = datetime.datetime.combine(datetime.date.today(), start_time)

    while current_time.time() <= end_time:
        end_slot = (current_time + interval).time()
        hours.append(InlineKeyboardButton(f"{current_time.strftime('%H:%M')} - {end_slot.strftime('%H:%M')}",
                                          callback_data=f"select_time_{current_time.strftime('%H:%M')}_{end_slot.strftime('%H:%M')}"))
        current_time += interval

    confirm_button = InlineKeyboardButton("Cofnirm", callback_data="confirm_hour")

    keyboard = InlineKeyboardMarkup(row_width=2)
    for i in range(0, len(hours), 2):
        keyboard.row(*hours[i:i + 2])
    keyboard.add(confirm_button)
    return keyboard


def change_keyboard(selected_date, keyboard):
    new_keyboard = []
    for row in keyboard:
        new_row = []

        for button in row:
            button_text = button["text"]

            if button_text.startswith("🟢 ") and button_text[2:] == selected_date:
                button_text = button_text[2:]
            elif button_text == selected_date:
                button_text = f"🟢 {button_text}"

            new_button_text = button_text

            new_button = InlineKeyboardButton(text=new_button_text, callback_data=button["callback_data"])
            new_row.append(new_button)

        new_keyboard.append(new_row)
    return InlineKeyboardMarkup(inline_keyboard=new_keyboard)


def change_keyboard_time(selected_time_range, keyboard):
    new_keyboard = []
    for row in keyboard:
        new_row = []

        for button in row:

            button_text = button["text"]
            button_data = button["callback_data"].replace('select_time_', '')
            button_time_range = button_data.split(' - ')[0]

            if button_text.startswith("🟢 ") and button_time_range == selected_time_range:
                button_text = button_text.replace("🟢 ", '')
            elif button_time_range == selected_time_range:
                button_text = f"🟢 {button_text}"

            new_button = InlineKeyboardButton(text=button_text, callback_data=button["callback_data"])
            new_row.append(new_button)

        new_keyboard.append(new_row)

    return InlineKeyboardMarkup(inline_keyboard=new_keyboard)


def free_times_list_create():
    data = get_free_time()
    # print(data)
    keyboard = InlineKeyboardMarkup()
    added_users = set()

    for item in data:
        user_id = item.get('user_id')

        if user_id not in added_users:
            user = get_user(user_id)
            name = user.get('name')
            button_text = f"{name} - {user_id}"
            callback_data = f"view_free_times_{user_id}"

            keyboard.add(InlineKeyboardButton(button_text, callback_data=callback_data))
            added_users.add(user_id)

    return keyboard


def create_free_time_keyboard(user_id):
    data = get_free_time()
    user_free_times = [item for item in data if item['user_id'] == user_id]

    keyboard = InlineKeyboardMarkup()
    for free_time in user_free_times:
        time_data = free_time['time_data']
        days = time_data['days'].split('\n')
        hours = time_data['hours'].split('\n')

        for day, hour in zip(days, hours):
            button_text = f"{day} - {hour.replace('_', ' - ')}"
            callback_data = f"select_free_time_{day}T{hour.split('_')[0]}:00Z_{user_id}"
            keyboard.add(InlineKeyboardButton(button_text, callback_data=callback_data))
    keyboard.add(InlineKeyboardButton("Back", callback_data="view_free_times"))

    return keyboard


days_list = create_days_list()
hours_list = create_hours_list()
