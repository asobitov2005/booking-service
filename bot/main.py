import json
from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.utils import executor

from bot.send_mail import send_mail
from bot.zoom_api import *
from config import *
from keyboard import *
from states import *
from aiogram.contrib.middlewares.logging import *
from aiogram.dispatcher import FSMContext

storage = MemoryStorage()
bot = Bot(token=BOT_TOKEN)
dp = Dispatcher(bot, storage=storage)
dp.middleware.setup(LoggingMiddleware())


@dp.message_handler(commands=['start'])
async def start(message: types.Message):
    print(create_user(name=message.from_user.full_name, user_id=message.from_user.id))
    user = get_user(message.from_user.id)
    if user.get('email') is not None:
        await message.answer(text="Select one of the following buttons:", reply_markup=menu)
    else:
        await BotState.waiting_email.set()
        await message.answer(text="Enter your email:")


@dp.message_handler(state=BotState.waiting_email)
async def waiting_email(message: types.Message, state: FSMContext):
    email = message.text
    text = ''
    if email.count('@') == 1 and '.' in email.split('@')[-1]:
        put_mail(message.from_user.id, email)
        await message.answer("Email saved successfully")
        await main_menu(dp, message.chat.id)
        await state.finish()
    else:
        await message.answer("Invalid email format! Please enter a valid email.")


@dp.callback_query_handler(lambda c: c.data == 'create_meeting')
async def create_meeting(callback_query: types.CallbackQuery):
    new_text = "Enter your days off"
    back = InlineKeyboardButton("Back", callback_data="menu")
    await bot.edit_message_text(
        new_text,
        callback_query.message.chat.id,
        callback_query.message.message_id,
        reply_markup=days_list
    )


@dp.callback_query_handler(lambda c: c.data == 'menu')
async def go_menu(callback_query: types.CallbackQuery):
    new_text = "Select one of the following buttons:"
    await bot.edit_message_text(
        new_text,
        callback_query.message.chat.id,
        callback_query.message.message_id,
        reply_markup=menu
    )


@dp.callback_query_handler(lambda c: c.data.startswith('select_date_'))
async def handle_select_date(callback_query: types.CallbackQuery):
    selected_date = callback_query.data.replace('select_date_', '')
    old_keyboard = callback_query.message.reply_markup.inline_keyboard
    new_keyboard = change_keyboard(selected_date, old_keyboard)
    text = callback_query.message.text

    if text == "Enter your days off":
        text = "Selected days off:\n" + selected_date
    else:
        split_text = text.split("\n")
        edit_text = True
        for word in split_text:
            if word == selected_date:
                split_text.remove(word)
                text = '\n'.join(split_text)
                edit_text = False
                break
        if edit_text:
            text = text + "\n" + selected_date

    await bot.edit_message_text(
        text,
        callback_query.message.chat.id,
        callback_query.message.message_id,
        reply_markup=new_keyboard
    )


@dp.callback_query_handler(lambda c: c.data.startswith('select_time_'))
async def handle_select_time(callback_query: types.CallbackQuery):
    selected_time_range = callback_query.data.replace('select_time_', '')
    old_keyboard = callback_query.message.reply_markup.inline_keyboard
    new_keyboard = change_keyboard_time(selected_time_range.split(' - ')[0], old_keyboard)
    text = callback_query.message.text.replace("Choose your free time", "Selected free time:")
    split_text = text.split("\n")
    edit_text = True
    for word in split_text:
        if word == selected_time_range:
            split_text.remove(word)
            text = '\n'.join(split_text)
            edit_text = False
            break
    if edit_text:
        text = text + "\n" + selected_time_range
    await bot.edit_message_text(
        text,
        callback_query.message.chat.id,
        callback_query.message.message_id,
        reply_markup=new_keyboard
    )


@dp.callback_query_handler(lambda c: c.data.startswith('select_free_time_'))
async def select_free_time_(callback_query: types.CallbackQuery):
    await bot.edit_message_text(
        "Creating....",
        callback_query.message.chat.id,
        callback_query.message.message_id
    )
    time_and_user_id = callback_query.data.replace('select_free_time_', '').split('_')
    start_time = time_and_user_id[0]
    # print(start_time)
    user_id_of_free_time = time_and_user_id[-1]
    # print(user_id_of_free_time)
    user = get_user(user_id_of_free_time)
    first_person = user.get('name')
    first_person_email = user.get('email')
    second_person_email = get_user(callback_query.message.chat.id).get('email')
    second_person = callback_query.message.chat.full_name
    meeting_name = f"{first_person} is meeting with {second_person}"
    token = fetch_bearer_token(client_id, client_secret, account_id)
    duration = 60  # minutes

    zoom = create_zoom_meeting(token, meeting_name, start_time, duration)
    if zoom:
        meeting_id = zoom['id']
        meeting_url = zoom['join_url']
        new_text = f"Meeting created successfully:\nMeeting name: {meeting_name}\nMeeting ID: {meeting_id}\nMeeting URL: {meeting_url}\nMeeting Duration: {duration} minutes\nStart time: {start_time.replace('T', ' ').replace('Z', '')}"
        await bot.edit_message_text(
            new_text,
            callback_query.message.chat.id,
            callback_query.message.message_id
        )
        await bot.send_message(
            user_id_of_free_time,
            new_text
        )
        subject = 'Meeting'
        message = new_text
        email_from = "asobitov2005@gmail.com"
        recipient_list = [first_person_email, second_person_email]
        send_mail(subject, message, email_from, recipient_list,
                  fail_silently=False)


@dp.callback_query_handler(lambda c: c.data == 'confirm_date')
async def confirm_date(callback_query: types):
    text = callback_query.message.text
    if (text == "Enter your days off") or (text == "Selected days off:"):
        await callback_query.answer("You haven't chosen a date yet!", show_alert=True)
    else:
        text = callback_query.message.text
        new_text = text + "\n\n" + "Choose your free time"

        await bot.edit_message_text(
            new_text,
            callback_query.message.chat.id,
            callback_query.message.message_id,
            reply_markup=hours_list
        )


@dp.callback_query_handler(lambda c: c.data == 'confirm_hour')
async def confirm_hour(callback_query: types.CallbackQuery, state: FSMContext):
    text = callback_query.message.text
    await state.update_data(text=text)
    new_text = text + "\n\n" + "<b>Enter your email:</b>"
    if check_hours_availability(text):
        await bot.edit_message_text(
            new_text,
            callback_query.message.chat.id,
            callback_query.message.message_id,
            parse_mode="HTML"
        )
        await BotState.waiting_for_email.set()
    else:
        await callback_query.answer("You haven't chosen a time yet!", show_alert=True)


async def save_data(text, email, message):
    text = text.replace("Selected days off:\n", '').replace("Selected free time:\n", '')
    text = text.split("\n\n")
    days = text[0]
    hours = text[1]
    time_data = {
        "days": f"{days}",
        "hours": f"{hours}"
    }
    print(put_mail(message.chat.id, email))
    time_data = json.dumps(time_data)
    print(free_time_create(message.chat.id, time_data))


@dp.message_handler(state=BotState.waiting_for_email)
async def process_email(message: types.Message, state: FSMContext):
    email = message.text
    text = ''
    if email.count('@') == 1 and '.' in email.split('@')[-1]:
        async with state.proxy() as data:
            text += data.get('text')
        await save_data(text, email, message)
        await message.answer(f"Your free time has been created ✅\n\n{text}<b>\n\nEmail:</b> <code>{email}</code>",
                             parse_mode="HTML")
        await main_menu(dp, message.chat.id)
        await state.finish()
    else:
        await message.answer("Invalid email format! Please enter a valid email.")


@dp.callback_query_handler(lambda c: c.data == 'view_free_times')
async def view_free_times(callback_query: types.CallbackQuery):
    new_text = "Choose one of the users"
    keyboard = free_times_list_create()
    await bot.edit_message_text(
        new_text,
        callback_query.message.chat.id,
        callback_query.message.message_id,
        reply_markup=keyboard
    )


@dp.callback_query_handler(lambda c: c.data.startswith('view_free_times_'))
async def view_free_times_by_user_id(callback_query: types.CallbackQuery):
    user_id = callback_query.data.replace("view_free_times_", '')
    user = get_user(user_id)
    name = user.get('name')
    keyboard = create_free_time_keyboard(user_id)
    new_text = f"{name}'s free times"
    await bot.edit_message_text(
        new_text,
        callback_query.message.chat.id,
        callback_query.message.message_id,
        reply_markup=keyboard
    )


def check_hours_availability(text):
    index = text.find("Choose your free time")

    if index == -1:
        return True

    hours_part = text[index + len("Choose your free time"):].strip()

    if not hours_part:
        return False
    return True


async def on_start_notify(dsp: Dispatcher):
    try:
        await dsp.bot.send_message(chat_id=ADMIN, text="The bot has started")
        print('The bot has started')
    except Exception as e:
        print("Error:", e)


async def on_start(dpr: Dispatcher):
    await on_start_notify(dpr)


async def main_menu(dsp: Dispatcher, chat_id):
    try:
        await dsp.bot.send_message(chat_id=chat_id, text="Select one of the buttons below", reply_markup=menu)
    except Exception as e:
        print("Error:", e)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_start)
