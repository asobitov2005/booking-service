import base64
import requests


def fetch_bearer_token(CLIENT_ID, CLIENT_SECRET, ACCOUNT_ID):
    credentials = base64.b64encode(f"{CLIENT_ID}:{CLIENT_SECRET}".encode()).decode()
    token_url = f"https://zoom.us/oauth/token?grant_type=account_credentials&account_id={ACCOUNT_ID}"
    headers = {
        'Authorization': f'Basic {credentials}',
        'Content-Type': 'application/x-www-form-urlencoded',
    }
    response = requests.post(token_url, headers=headers)
    if response.status_code == 200:
        return response.json()['access_token']
    else:
        raise Exception(f"Error fetching bearer token: {response.text}")


def create_zoom_meeting(token, topic, start_time, duration):
    url = 'https://api.zoom.us/v2/users/me/meetings'
    headers = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json'
    }
    data = {
        'topic': topic,
        'type': 2,
        'start_time': start_time,
        'duration': duration
    }
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 201:
        return response.json()
    else:
        print(f"Error: {response.status_code}, {response.text}")
        return None


# topic = 'Sample Meeting'
# start_time = '2024-03-28T12:00:00Z'
# duration = 60
#
# meeting_info = create_zoom_meeting(token, topic, start_time, duration)
# if meeting_info:
#     print("Meeting created successfully!")
#     print("Meeting ID:", meeting_info['id'])
#     print("Join URL:", meeting_info['join_url'])
# else:
#     print("Failed to create meeting.")
