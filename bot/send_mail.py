import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from bot.config import EMAIL_USER, EMAIL_PASSWORD


def send_mail(subject, message, email_from, recipient_list, fail_silently=False):
    msg = MIMEMultipart()
    msg['From'] = email_from
    msg['To'] = ', '.join(recipient_list)
    msg['Subject'] = subject

    msg.attach(MIMEText(message, 'plain'))

    smtp_server = smtplib.SMTP('smtp.gmail.com', 587)
    smtp_server.starttls()

    smtp_server.login(EMAIL_USER, EMAIL_PASSWORD)

    smtp_server.sendmail(email_from, recipient_list, msg.as_string())

    smtp_server.quit()


