from rest_framework import viewsets, generics
from .models import BotUser, FreeTime, Meeting
from .serializers import BotUserSerializer, FreeTimeSerializer, MeetingSerializer


class BotUserViewSet(viewsets.ModelViewSet):
    queryset = BotUser.objects.all()
    serializer_class = BotUserSerializer


class FreeTimeViewSet(viewsets.ModelViewSet):
    queryset = FreeTime.objects.all()
    serializer_class = FreeTimeSerializer


class MeetingViewSet(viewsets.ModelViewSet):
    queryset = Meeting.objects.all()
    serializer_class = MeetingSerializer


class UserDetailView(generics.RetrieveAPIView):
    queryset = BotUser.objects.all()
    serializer_class = BotUserSerializer
    lookup_field = 'user_id'


