from rest_framework import serializers

from .models import BotUser, FreeTime, Meeting


class BotUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = BotUser
        fields = '__all__'


class FreeTimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FreeTime
        fields = '__all__'


class MeetingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meeting
        fields = '__all__'
