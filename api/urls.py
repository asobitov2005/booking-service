from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import BotUserViewSet, FreeTimeViewSet, MeetingViewSet, UserDetailView

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'bot-users', BotUserViewSet, basename='bot-user')
router.register(r'free-times', FreeTimeViewSet, basename='free-time')
router.register(r'meetings', MeetingViewSet, basename='meeting')

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
    path('users/<str:user_id>/', UserDetailView.as_view(), name='user-detail'),
]
