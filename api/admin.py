from django.contrib import admin

from api.models import *

# Register your models here.
admin.site.register(FreeTime)
admin.site.register(Meeting)
admin.site.register(BotUser)
