from django.db import models


class BotUser(models.Model):
    user_id = models.CharField(max_length=120)
    name = models.CharField(max_length=120)
    email = models.EmailField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class FreeTime(models.Model):
    user_id = models.CharField(max_length=120)
    time_data = models.JSONField()

    def __str__(self):
        return f"{self.user_id}"


class Meeting(models.Model):
    name = models.CharField(max_length=120)
    day = models.CharField(max_length=50)
    start_time = models.TimeField()
    end_time = models.TimeField()
    first_person = models.IntegerField()
    second_person = models.IntegerField()
    link = models.URLField(null=True)

    def __str__(self):
        return f'{self.name} - {self.start_time} - {self.end_time}'

