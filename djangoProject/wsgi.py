import os
import django

from django.core.wsgi import get_wsgi_application

# Set the DJANGO_SETTINGS_MODULE environment variable
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'djangoProject.settings')

# Configure Django settings
django.setup()

# Get the WSGI application
application = get_wsgi_application()
